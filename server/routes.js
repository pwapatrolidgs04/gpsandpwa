// Routes.js - Módulo de rutas
const express = require('express');
const router = express.Router();

//tareasDataPeticionesGet
const tareasData = [
  {id:1, text:'Tarea PWA', completed:false},
  {id:2, text:'Tarea GPS', completed:true},
  {id:3, text:'Tarea Video Juegos', completed:false},
  {id:4, text:'Tarea Movil', completed:false},
  {id:5, text:'Tarea Integradora', completed:false},
  {id:6, text:'Tarea Inglés', completed:true},
  {id:7, text:'Encuesta', completed:true},
  {id:8, text:'Platica', completed:false}
 
]
// Get mensajes
router.get('/', function (req, res) {
  res.json(tareasData);
});

//get
router.get('/:id', function (req, res) {
  const idTarea = Number(req.params.id);
  const tarea = tareasData.find((el) => el.id == idTarea);
  res.json(tarea);
});

//Post mensaje 
router.post('/', function (req, res) {
  const newTareaValue = {
    id: tareasData[tareasData.length - 1].id + 1,
    text: req.body.newTareaValue,
    completed: false,
  };
  tareasData.push( newTareaValue );
  console.log(tareasData);

  res.json({
    ok: true,
    newTareaValue
  });
});

/************************************ */
//put
router.post('/:id', function (req, res) {
  const Id = Number(req.params.id);
  console.log(req.body);
  const nuevaTarea = req.body.actualizarTarea;
  const tarea = tareasData.find((tarea) => tarea.id === Id);
  const index = tareasData.indexOf(tarea);

  if (!tarea) {
    res.status(500).send('Tarea no encontrada');
  } else {
    const actualizar = { ...tarea, ...nuevaTarea };
    tareasData[index] = actualizar;
    res.json({
      ok: true,
      actualizar,
    });
  }
});

//Delete
router.delete('/:id', function (req, res) {
  const idTarea = Number(req.params.id);

  const tarea = tareasData.find((el) => el.id == idTarea);

  if (!tarea) {
    res.status(500).send('Tarea no encontrada');
  } else {
    const indiceElemento = tareasData.findIndex((el) => el.id == idTarea);
    tareasData.splice(indiceElemento, 1);

    res.json(tareasData);
  }
});


/************************************ */

module.exports = router;
