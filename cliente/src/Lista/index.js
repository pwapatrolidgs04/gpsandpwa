import React from 'react';
import '../App.css';
import './Lista.css';

function Lista(props) {
  return (
    <table>
    {props.children}
    </table>
  )
}

export { Lista }
