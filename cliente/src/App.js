//import logo from './logo.svg';
//import { Boton } from './Boton';
//import Container from '@mui/material/Container';
import './App.css';
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Buscador } from './Buscador/index';
import { Item } from './Item/index';
import { Lista } from './Lista/index';
import { Formulario } from './Formulario/index';
import { Contador } from './Contador/index';


function App() {
  const notificarme = () => {
    if (!window.Notification) {
      console.log('Este navegador soporta notificaciones');
      return
    }
    if (Notification.permission === 'granted') {
      
    } else if (Notification.permission !== 'denied' || Notification.permission === 'default'){
      Notification.requestPermission((permission) => {
        console.log(permission);
        if (permission === 'granted') {
          new Notification('NotApp te da la Bienvenida');
        }
      })
    }
  }

  notificarme();
  
/**comanetado para avnazar a funciones get y post 
  //------Persistencia de datos---------
  const localStorageTareas = localStorage.getItem('TAREAS_V1');
  let parsedTareas;

  if (!localStorageTareas) {
    localStorage.setItem('TAREAS_V1', JSON.stringify([]));
  } else {
    parsedTareas = JSON.parse(localStorageTareas);
  }

  //-------guardar tareas en el estado-------
  const guardarTareas = (newTarea) => {
    const stringTareas = JSON.stringify(newTarea);
    localStorage.setItem('TAREAS_V1', stringTareas);
    setTareas(newTarea);
  };

  // C O N T A D O R
  //Estado
  const [tareas, setTareas] = React.useState(parsedTareas);
  // Variable de tareas completadoas
  const completedTareas = tareas.filter((tarea) => !!tarea.completed).length;
  // Variable que almacena el total de tareas
  const totalTareas = tareas.length;

  //Crear la función --> Completar tarea
  // eslint-disable-next-line no-unused-vars
  const completarTarea = (text) => {
    // eslint-disable-next-line eqeqeq
    const tareaIndex = tareas.findIndex((tarea) => tarea.text == text);
    const nuevasTareas = [...tareas];
    nuevasTareas[tareaIndex].completed = true;
    guardarTareas(nuevasTareas);
  };

  // BUSCADOR

  //Crear el estado
  const [buscandoValor, setBuscandoValor] = React.useState('');

  //Crear un array vacio
  let buscarTareas = [];

  //Condicional
  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tarea) => {
      const tareaText = tarea.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  //Crear la función --> Eliminar tarea
  // eslint-disable-next-line no-unused-vars
  const eliminarTarea = (text) => {
    // eslint-disable-next-line eqeqeq
    const tareaIndex = tareas.findIndex((tarea) => tarea.text == text);
    const nuevasTareas = [...tareas];
    nuevasTareas.splice(tareaIndex, 1);
    guardarTareas(nuevasTareas);
  };
------------Fin de comentario para avance get y post*/

 //1.- Crear useState
 const [tareas, setTareas] = useState([])

  //Petición get
  const obtenerTareas = async () => {
    const url = 'api';
    const result = await axios.get(url);
    setTareas(result.data);
  };

  // Crear useEffect
  useEffect(() => {
    obtenerTareas();
  }, [tareas])
  

  const TareasCompletadas = tareas.filter((tarea) => !!tarea.completed).length;
  const TareasTotal = tareas.length;

  const [buscandoValor, setBuscandoValor] = useState('');

  let buscarTareas = [];

  if (!buscandoValor.length >= 1) {
    buscarTareas = tareas;
  } else {
    buscarTareas = tareas.filter((tarea) => {
      const tareaText = tarea.text.toLowerCase();
      const buscarText = buscandoValor.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  const saveTareas = (nuevasTareas) => {
    const stringTareas = JSON.stringify(nuevasTareas);
    localStorage.setItem('tareas-1', stringTareas);
    setTareas(nuevasTareas);
  };

  
  const addTarea = (tarea) => {
    const newTarea = [...tareas];
    newTarea.push({
      completed: false,
      tarea,
    });
    saveTareas(newTarea);
  };

  //Completar tareas
  //**************************** */
  const completarTarea = (id, text, completed, e) => {
    let comple;

    if (completed !== true) {
      comple = true;
    } else {
      comple = false
    }

    const actualizarTarea = {
      text: text,
      completed: comple
    }
    console.log(actualizarTarea);
    e.preventDefault();
    axios
      //PUT
      .post(`api/${id}`, {
        actualizarTarea
      })
        .then((res) => console.log('Petición Put', res))
          .catch((err) => console.log(err));
  };

  const postDelete = (id, e) => {
    e.preventDefault();
    axios.delete(`api/${id}`)
      .then(res => console.log('Delete', res))
        .catch(err => console.log(err))
  }

//**************************** */
  return (
    <div className='App'>
      <h1>Mis Tareas</h1>
      <Contador total={TareasTotal} completadas={TareasCompletadas} />
      <Buscador
       buscandoValor={buscandoValor}
       setBuscandoValor={setBuscandoValor}
      />
      <Formulario crear={addTarea} />
      <Lista>
        {buscarTareas.map((tarea) => (
          <Item
            text={tarea.text}
            key={tarea.text}
            completar={tarea.completed}
            onComplete={(e) => completarTarea(tarea.id, tarea.text, tarea.completed, e)}
            onUpdate={tarea.id}
            onDelete={(e) => postDelete(tarea.id, e)}
          />
        ))}
      </Lista>
    </div>
/**comentario para que no estorbe mientras hago funcion get
     * integrar despues
     
    <Container
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'column',
        width: '100vw',
        height: '100vh',
      }}
    >
      <div
        style={{
          backgroundColor: '#8477C8',
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
          padding: '30px',
          borderRadius: '10px',
        }}
      >
        <Contador total={totalTareas} completed={completedTareas} />
        <Buscador
          buscandoValor={buscandoValor}
          setBuscandoValor={setBuscandoValor}
        />
        <Lista>
          {buscarTareas.map((tarea) => {
            //console.log(tarea.text);
            return (
              <Item
                text={tarea.text}
                completed={tarea.completed}
                //completarTarea={completarTarea}
                onComplete={() => completarTarea(tarea.text)}
                borrar={() => eliminarTarea(tarea.text)}
                key={tarea.text}
              />
            );
          })}
        </Lista>
        <Boton />
      </div>
    </Container>
    */
  
  );
}

export default App;
