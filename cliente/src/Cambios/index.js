import React, { useState } from 'react';
import '../App.css';
import './Cambios.css';
import EditIcon from '@mui/icons-material/Edit';
import Modal from 'react-modal';
import axios from 'axios';

const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)',
  },
};

function Cambios(props) {

  let subtitle;
  const [modalIsOpen, setIsOpen] = React.useState(false);

  function openModal() {
    setIsOpen(true);
  }

  function afterOpenModal() {
    subtitle.style.color = '#f00';
  }

  function closeModal() {
    setIsOpen(false);
  }


  const [newTareaValue, setNewTareaValue] = useState('');

  let updateTarea = [];

  if (!updateTarea.length >= 1) {
    updateTarea = newTareaValue;
  } else {
    updateTarea = newTareaValue.filter((tarea) => {
      const tareaText = tarea.text.toLowerCase();
      const buscarText = updateTarea.toLowerCase();

      return tareaText.includes(buscarText);
    });
  }

  const onChange = (event) => {
    setNewTareaValue(event.target.value);
  };

  const onUpdate = (event) => {
    console.log(props.comple);
    const actualizarTarea = {
      text: newTareaValue,
      completed: props.comple
    }
    event.preventDefault();
    axios
      //POST
      .post(`api/${props.idTarea}`, {
        actualizarTarea
      })
      .then((res) => console.log('Petición Put', res))
      .catch((err) => console.log(err));
  };

  const obtenerTarea = (e) => {
    e.preventDefault();
    const result =  axios.get(`api/${props.idTarea}`)
        .then(res => console.log('Tarea', res))
          .catch(err => console.log(err))
    setNewTareaValue(result.data);
  }

  return (
    <>
      <div className='parent2'>
        <button onClick={openModal}>
          <EditIcon />
        </button>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          style={customStyles}
          contentLabel='Example Modal'
        >
          <h2 ref={(_subtitle) => (subtitle = _subtitle)}>
            Actualizar Nota
          </h2>
            
            <form onSubmit={onUpdate}>
              <input name='tarea'value={newTareaValue} onChange={onChange} />
              <br/>
              <button onClick={closeModal}>
                close
              </button>
              <button type='submit'>
                Actualizar
              </button>
            </form>

        </Modal>
      </div>
    </>
  );
}
export { Cambios };
