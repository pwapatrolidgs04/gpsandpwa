/* eslint-disable no-undef */
import React from 'react';
import '../App.css';
import './Item.css';
import Check from "@mui/icons-material/Check";
import Delete from "@mui/icons-material/Delete";
import { Cambios } from './../Cambios/index';
//
/**
import IconButton from '@mui/material/IconButton';
import { Box } from '@mui/material';
*/
function Item(props) {
  return (
    <>
    <tr className="TodoItem">
    <td><button className="button button1" onClick={props.onComplete}><Check /></button></td>
    <td className={`TodoItem-p ${props.completar && 'TodoItem-p--complete'}`}>{props.text}</td>
    <td><button className="button button2" onClick={props.onDelete}><Delete /></button></td>
    <td><Cambios comple={props.completar} idTarea={props.onUpdate}/></td>
    </tr>
    </>

    /**COMENTADO PARA QUE NO ESTORBE EL LA PETICION GET INTEGRA DESPUES
    <li
      style={{
        display: 'flex',
        backgroundColor: '#ECE5E5',
        alignContent: 'center',
        justifyContent: 'center',
        marginBottom: '10px',
        borderRadius: '10px',
      }}
    >
      <Box>
        <IconButton
          completed={props.completar}
          onComplete={() => props.completarTarea(props.text)}
          onClick={props.onComplete}
        >
          <CheckIcon />
        </IconButton>
      </Box>
      <Box>
        <p>{props.text}</p>
      </Box>
      <Box>
        <IconButton
          edge='end'
          aria-label='delete'
          //completed={props.completar}
          borrar={() => props.eliminarTarea(props.text)}
          onClick={props.borrar}
        >
          <DeleteIcon />
        </IconButton>
      </Box>
    </li> */
  )
}

export { Item };
