//import React from 'react';
import React, {useState} from 'react';
import  Modal from 'react-modal';
import Plus from '@mui/icons-material/ControlPoint';
import './formulario.css';
import '../App.css';
import axios from 'axios'

const customStyles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };

function Formulario(props) {
    let subtitle;
    const [modalIsOpen, setIsOpen] = React.useState(false);

    function openModal() {
    setIsOpen(true);
    }

    function afterOpenModal() {
      subtitle.style.color = 'B148F6';
  }
  
    function closeModal() {
      setIsOpen(false);
    }
    
    const [newTareaValue, setNewTareaValue] = useState('');

    const onChange = (event) => {
      setNewTareaValue(event.target.value);
    };
 
    const onSubmit = (event) => {
      event.preventDefault();
      props.crear(newTareaValue);
      axios
        //POST
        .post('api', {
          newTareaValue
        })
        .then((res) => console.log('Petición Post', res))
        .catch((err) => console.log(err));
    };
    
    return(
        <>
        <div className='parent'>
          <button onClick={openModal}>
            Agregar Tarea  <Plus />
          </button>
          <Modal
            isOpen={modalIsOpen}
            onAfterOpen={afterOpenModal}
            onRequestClose={closeModal}
            style={customStyles}
            contentLabel='Example Modal'
          >
            <h2 ref={(_subtitle) => (subtitle = _subtitle)}>Agragar Tarea</h2>
            <form onSubmit={onSubmit}>
              <input value={newTareaValue} onChange={onChange} />
              <br />
              <button onClick={closeModal}>close</button>
              <button type='submit'>crear</button>
            </form>
          </Modal>
        </div>
      </>
    );
}

export {Formulario};