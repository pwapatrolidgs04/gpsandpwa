import React from 'react';
import '../App.css';
import './Boton.css';

function Boton() {
  return (
    <button
      onClick={() => {
        alert('Se guardo correctamente su tarea');
      }}
    >
      Crear tarea
    </button>
  );
}

export { Boton };
