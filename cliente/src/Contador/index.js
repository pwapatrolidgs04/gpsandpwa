import React from 'react';
import '../App.css';
import './Contador.css';

function Contador({ total, completadas }) {
  return (
    <h2>
      Has completado: {completadas} de {total} tareas
    </h2>
  );
}

export { Contador };
