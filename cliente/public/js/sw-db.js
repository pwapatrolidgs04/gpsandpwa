// Utilidades para grabar PouchDB INVESTIGAR QUE ONDA CON POUCHDB
// eslint-disable-next-line no-undef
const db = new PouchDB('mensajes');


// eslint-disable-next-line no-unused-vars
function guardarMensaje( mensaje ) {

    mensaje._id = new Date().toISOString();

    return db.put( mensaje ).then( () => {
        // eslint-disable-next-line no-restricted-globals
        self.registration.sync.register('nuevo-post');

        const newResp = { ok: true, offline: true };

        return new Response( JSON.stringify(newResp) );

    });

}


// Postear mensajes a la API
// eslint-disable-next-line no-unused-vars
function postearMensajes() {

    const posteos = [];

    return db.allDocs({ include_docs: true }).then( docs => {


        docs.rows.forEach( row => {

            const doc = row.doc;

            const fetchPom =  fetch('api', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify( doc )
                }).then( res => {

                    return db.remove( doc );

                });
            
            posteos.push( fetchPom );


        }); // fin del foreach

        return Promise.all( posteos );

    });





}

