// Guardar  en el cache dinamico
function actualizaCacheDinamico(cacheDinamico, req, res) {
  if (res.ok) {
    return caches.open(cacheDinamico).then((cache) => {
      cache.addAll(req, res.clone());

      return res.clone();
    });
  } else {
    return res;
  }
}

// eslint-disable-next-line no-unused-vars
function actualizaCacheEstatico(estaticoCache, req, APP_SHELL_INMUTABLE) {
  if (APP_SHELL_INMUTABLE.includes(req.url)) {
    // No hace falta actualizar el inmutable
    // console.log('existe en inmutable', req.url );
  } else {
    // console.log('actualizando', req.url );
    return fetch(req).then((res) => {
      return actualizaCacheDinamico(estaticoCache, req, res);
    });
  }
}

//Estrategia de cahe Red y actualización de cache

// eslint-disable-next-line no-unused-vars
function manejoApi(cacheName, req) {
  //Peticiones Post del manejo de API
  if (req.clone().method === 'POST') {
    if (self.ServiceWorkerRegistration.sync) {
      return req
        .clone()
        .text()
        .then((body) => {
          const bodyObj = JSON.parse(body);
          // eslint-disable-next-line no-undef
          return guardarMensaje(bodyObj);
        });
    } else {
      return fetch(req);
    }
  } else {
    return fetch(req)
      .then((res) => {
        if (res.ok) {
          actualizaCacheDinamico(cacheName, req, res.clone());
          return res.clone();
        } else {
          return caches.match(req);
        }
      })
      .catch((err) => {
        return caches.match(req);
      });
  }
}
