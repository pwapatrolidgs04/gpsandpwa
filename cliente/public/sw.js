// imports
// eslint-disable-next-line no-undef
importScripts('https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js');
// eslint-disable-next-line no-undef
importScripts('js/sw-db.js');
// eslint-disable-next-line no-undef
importScripts('js/sw-utils.js');

//Crear constantes para almacenar el cahe
const ESTATICO_CACHE = 'static-v3';
const DINAMICO_CACHE = 'dinamico-v1';
const INMUTABLE_CACHE = 'inmutable-v1';

const APP_SHELL = [
  '/',
  'index.html',
  'style.css',
  'js/app.js',
  'js/sw-utils.js',
  'js/sw-db.js',
  'manifest.json',
];

const APP_SHELL_INMUTABLE = [
  //'https://fonts.googleapis.com/css2?'
  'https://cdn.jsdelivr.net/npm/pouchdb@7.0.0/dist/pouchdb.min.js',
];
//Proceso de instalacion
// eslint-disable-next-line no-restricted-globals
self.addEventListener('install', (event) => {
  const cacheStatic = caches
    .open(ESTATICO_CACHE)
    .then((cache) => cache.addAll(APP_SHELL));
  const cacheInmutable = caches
    .open(INMUTABLE_CACHE)
    .then((cache) => cache.addAll(APP_SHELL_INMUTABLE));

  event.waitUntil(Promise.all[(cacheStatic, cacheInmutable)]);
});

//Proceso de activacion
// eslint-disable-next-line no-restricted-globals
self.addEventListener('activate', (event) => {
  //Eliminar cache del sw anterior
  const respuesta = caches.keys().then((keys) => {
    keys.forEach((key) => {
      if (key !== ESTATICO_CACHE && key.includes('static')) {
        return caches.delete(key);
      }
    });
  });

  event.waitUntil(respuesta);
});

//Estrategia de cache
// eslint-disable-next-line no-restricted-globals
self.addEventListener('fetch', (event) => {
  let respuesta;
  //Cambio en estrategia de cache
  if (event.request.url.includes('/api')) {
    //Tenemos que enviar una respuesta
    // eslint-disable-next-line no-undef
    respuesta = manejoApi(DINAMICO_CACHE, event.request);
  } else {
    respuesta = caches.match(event.request).then((res) => {
      if (res) {
        // eslint-disable-next-line no-undef
        actualizaCacheEstatico(
          ESTATICO_CACHE,
          event.request,
          APP_SHELL_INMUTABLE
        );
        return res;
      } else {
        return fetch(event.request).then((newRes) => {
          // eslint-disable-next-line no-undef
          return actualizaCacheDinamico(DINAMICO_CACHE, event.request, newRes);
        });
      }
    });
  }

  event.respondWith(respuesta);
});

// tareas asíncronas
// eslint-disable-next-line no-restricted-globals
self.addEventListener('sync', (event) => {
  console.log('SW: Sync');

  if (event.tag === 'nuevo-post') {
    // postear a BD cuando hay conexión
    // eslint-disable-next-line no-undef
    const respuesta = postearMensajes();

    event.waitUntil(respuesta);
  }
});
